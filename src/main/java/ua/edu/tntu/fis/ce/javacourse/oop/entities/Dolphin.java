package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Dolphin extends Fish {


	public Dolphin(double weight, String name, int speed, int health, Image image) {
		super(weight, name, speed, health, image, Gender.MALE);
		System.out.println("Dolphin " + name + "creating ");
	}
	@Override
	public void swim(int longitude, int latitude) {
		System.out.printf("I'm swimming as a Dolphin %s :-)\n", this.getName());
	}

}
