package ua.edu.tntu.fis.ce.javacourse.oop.gamelogic;

import java.awt.Image;
import java.awt.image.BufferedImage;

import ua.edu.tntu.fis.ce.javacourse.oop.entities.Crawable;
import ua.edu.tntu.fis.ce.javacourse.oop.entities.Fish;
import ua.edu.tntu.fis.ce.javacourse.oop.entities.Food;
import ua.edu.tntu.fis.ce.javacourse.oop.entities.Gender;
import ua.edu.tntu.fis.ce.javacourse.oop.entities.Salmon;
import ua.edu.tntu.fis.ce.javacourse.oop.entities.Shrimp;

public class SeaWorldRunner {

	public static void main(String[] args) {
		
		Image image = new BufferedImage(20, 20, BufferedImage.TYPE_INT_RGB);
		
		Fish fish1 = new Salmon(5.0, "Bob", 50, 100, image, Gender.FEMALE);

		Fish fish2 = new Salmon(4.0, "Alice", 50, 100, image, Gender.MALE);
		
		Crawable shrimp = new Shrimp(0.1, "Shrimpy", 30, 100, image, Gender.FEMALE);
		
		fish1.eat((Food)shrimp);
		
		fish1.bite(fish2);
		
		System.out.println(fish1);
		System.out.println(fish2);
		System.out.println(shrimp);

	}

}
