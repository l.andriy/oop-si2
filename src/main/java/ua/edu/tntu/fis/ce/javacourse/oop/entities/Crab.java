package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Crab extends Creatures implements Crawable, Predator {
	
	
	public Crab() {
		super();
	}

	public Crab(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Creating a crab");
	}

	@Override
	public void crawl(int longitude, int latitude) {
		System.out.println("I'm crawling crab");

	}

}
