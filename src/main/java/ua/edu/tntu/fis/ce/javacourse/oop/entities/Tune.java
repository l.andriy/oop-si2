package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Tune extends Fish {
	
	

	public Tune() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Tune(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Tune " + name + "creating ");
	}

	public Tune(int health) {
		super(health);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim(int longitude, int latitude) {
		System.out.printf("I'm swimming as a Tune %s :-)\n", this.getName());

	}

}
