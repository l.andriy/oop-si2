package ua.edu.tntu.fis.ce.javacourse.oop.entities;

public interface Swimmable {
	
	void swim(int longitude, int latitude);


}
