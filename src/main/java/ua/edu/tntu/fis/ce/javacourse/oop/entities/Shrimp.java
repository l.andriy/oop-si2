package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Shrimp extends Animal implements Crawable, Food, Harbivour {
	
	
	public Shrimp() {
		super();
	}

	public Shrimp(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Creating a shrimp");
	}

	@Override
	public void crawl(int longitude, int latitude) {
		System.out.printf("I'm crawling as a Shrimp %s :-)\n", this.getName());

	}

}
