package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;
import java.time.LocalDateTime;
import java.util.Iterator;

import jdk.jfr.consumer.RecordedClass;

public abstract class Animal {
	private int id = 0;
	private double weight;
	private String name;
	private int speed;
	private int health;
	private Stomach stomach;
	private Image image;
	private Gender gender;
	
	public record MyDataClass(int x, int y) {};
	
	public static class Stomach {
		private static final int NUMBER_OF_FISHES = 10;
		private Food[] food;
		private LocalDateTime[] foodEatingTime;
		private int foodCount = 0;

		// ініціалізація змінних
		// виділення пам'яті
		// якщо викликаємо конструктор нащадка, автоматично викликаються конструктори всіх батьківських класів (ланцюжком)
		public Stomach() {
			super();
			this.food = new Food[NUMBER_OF_FISHES];
			this.foodEatingTime = new LocalDateTime[NUMBER_OF_FISHES];
		}
	
		
		public int swallow(Food...food) {
			int health = 0;
			for (Food f: food) {
				if (notFull()) {
					food[foodCount++] = f;
					foodEatingTime[foodCount] = LocalDateTime.now();
					health += 10;
				};				
			}
			return health;
		}
		
		public boolean notFull() {
			return this.food.length < NUMBER_OF_FISHES;
		}
		
		public boolean isFull() {
		   return this.food.length == NUMBER_OF_FISHES;
		}

	}
	
	//default
	public Animal() {
		super();
	}
	// super class - Object
	public Animal(double weight, String name, int speed, int health, Image image, Gender gender) {
		super();
		this.id++;
		this.weight = weight;
		this.name = name;
		this.speed = speed;
		this.health = health;
		this.stomach = new Stomach();
		this.image = image;
		System.out.println("Creating an animal");
		this.gender = gender;
	}
	
	public void eat(Food...food) {
		for (Food f : food) {
		  this.health += this.stomach.swallow(food);
		  this.weight += ((Animal) f).getWeight();
		}		
	}
	
	public void bitten(Animal animal) {
		this.health -= 20;
		System.out.println("I was bitten by " + animal.getClass().getSimpleName() + " " + animal.getName());
	}
	
	public void bite(Animal animal) {
		animal.bitten(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
	
	@Override
	public String toString() {
//		return "Animal(" + this.getClass().getSimpleName() + ")[id=" + id + ", weight=" + weight + ", name=" + name + ", speed=" + speed + ", health=" + health
//				+ ", stomach=" + stomach + ", gender=" + gender.getSymbol() + "]";
		return "Animal(" + this.getClass().getSimpleName() + ")[id=" + id + ", weight=" + weight + ", name=" + name + ", speed=" + speed + ", health=" + health
				+ ", stomach=" + stomach + ", gender=" + gender.getSymbol() + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + health;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + speed;
		result = prime * result + ((stomach == null) ? 0 : stomach.hashCode());
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (health != other.health)
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (speed != other.speed)
			return false;
		if (stomach == null) {
			if (other.stomach != null)
				return false;
		} else if (!stomach.equals(other.stomach))
			return false;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight))
			return false;
		return true;
	}
	
}
