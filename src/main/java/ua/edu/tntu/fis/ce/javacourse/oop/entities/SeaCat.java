package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class SeaCat extends Animal implements Swimmable {


	public SeaCat(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("SeaCat " + name + "creating ");
	}
	@Override
	public void swim(int longitude, int latitude) {
		System.out.printf("I'm swimming as a SeaCat %s :-)\n", this.getName());
	}

}
