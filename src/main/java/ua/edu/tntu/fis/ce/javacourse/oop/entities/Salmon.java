package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Salmon extends Fish {
	
	

	public Salmon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Salmon(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Salmon " + name + "creating ");
	}

	public Salmon(int health) {
		super(health);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim(int longitude, int latitude) {
		System.out.printf("I'm swimming as a Лосось %s :-)\n", this.getName());

	}

	@Override
	public String toString() {
		return super.toString() + "[Лосось]";
	}
	
	

}
