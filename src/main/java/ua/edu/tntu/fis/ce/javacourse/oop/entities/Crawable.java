package ua.edu.tntu.fis.ce.javacourse.oop.entities;

public interface Crawable {
	void crawl(int longitude, int latitude);
	
}
