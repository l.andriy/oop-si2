package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public abstract class Fish extends Animal implements Food, Swimmable {
	
	public Fish() {}
	
	public Fish(int health) {
		this(0.0, "Some fish", 10, health, null, Gender.FEMALE);
	}
	
	public Fish(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
	}
	
}
