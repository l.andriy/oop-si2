package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public class Shark extends Fish {

	public Shark(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Shark " + name + "creating ");
	}

	@Override
	public void swim(int longitude, int latitude) {
	System.out.printf("I'm swimming as a Shark %s :-)\n", this.getName());

	}

}
