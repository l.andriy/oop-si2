package ua.edu.tntu.fis.ce.javacourse.oop.entities;

//public enum Gender {
//	MALE,
//	FEMALE
//}


public enum Gender {
	MALE('\u2642'),
	FEMALE('\u2640');
	
	private char genderSymbol;
	
	private Gender(char genderSymbol) {
		this.genderSymbol = genderSymbol;
	}
	
	public char getSymbol() {
		return this.genderSymbol;
	}
	
}