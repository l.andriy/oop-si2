package ua.edu.tntu.fis.ce.javacourse.oop.entities;

import java.awt.Image;

public abstract class Creatures extends Animal implements Crawable {

	public Creatures() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Creatures(double weight, String name, int speed, int health, Image image, Gender gender) {
		super(weight, name, speed, health, image, gender);
		System.out.println("Creature created");
	}

	
}
